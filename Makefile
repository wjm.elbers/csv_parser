all: test

init_modules:
ifeq (,$(wildcard go.mod))
	go mod init gitlab.com/wjm.elbers/csv_parser
endif

get_modules: init_modules
	go mod tidy

test: get_modules
	go test

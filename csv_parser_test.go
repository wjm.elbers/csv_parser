package csv_parser

import (
	"encoding/json"
	"fmt"
	"strings"
	"testing"
	"time"
)

type TestDataStruct struct {
	Column1 string    `csv:"idx=0"`
	Column2 int64     `csv:"idx=1"`
	Column3 float64   `csv:"idx=2"`
	Column4 time.Time `csv:"idx=3,format=2-Jan-2006"`
	Column5 string    `csv:"-"`
}

func TestImport(t *testing.T) {
	csvDataReader := strings.NewReader("\"test 1\",1,1.1,\"01-May-2020\"\n\"test 2\",2,2.2,\"02-May-2020\"")

	data := []TestDataStruct{}
	csvData, err := New().ParseReader(csvDataReader, &data, 0)
	if err != nil {
		t.Fatalf("No error expected, got: %s", err)
	}
	ValidateResult(t, csvData, 2)
}

func TestFileImport(t *testing.T) {
	data := []TestDataStruct{}
	csvData, err := New().ParseFile("test_data.csv", &data, 1)
	if err != nil {
		t.Fatalf("No error expected, got: %s", err)
	}
	ValidateResult(t, csvData, 2)
}

func ValidateResult(t *testing.T, data []*CsvRecord, expectedRows int) {
	if len(data) != expectedRows {
		t.Fatalf("Exepcted 2 data rows, got %d\n", len(data))
	}

	for _, csvDatum := range data {
		if csvDatum.Data == nil {
			printJson(csvDatum)
			t.Fatalf("Record output data cannot be nil\n")
		}
		if len(csvDatum.Errors) > 0 {
			printJson(csvDatum)
			t.Fatalf("Exepcted no errors, got %d errors\n", len(csvDatum.Errors))
		}
	}

}

func printJson(d interface{}) {
	jsonBytes, _ := json.MarshalIndent(d, "", " ")
	fmt.Printf("result: %s\n", jsonBytes)
}

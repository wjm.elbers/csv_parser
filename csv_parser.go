package csv_parser

import (
	"crypto/sha256"
	"encoding/csv"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const (
	tagName      = "csv"
	tagSeparator = ","
)

type CsvRecord struct {
	Record []string    `json:"input"`
	Sha256 string      `json:"input_sha256"`
	Data   interface{} `json:"output"`
	Errors []string    `json:"errors"`
	Ns     int64       `json:"ns"`
}

func (r *CsvRecord) appendError(err string) {
	r.Errors = append(r.Errors, err)
}

type CsvParser struct {
	comma rune
}

func New() *CsvParser {
	return &CsvParser{comma: ','}
}

func NewCustom(comma rune) *CsvParser {
	p := New()
	p.comma = comma
	return p
}

//Read csv records from filename and parse each record into the provided data interface
func (p *CsvParser) ParseFile(filename string, data interface{}, skipRows int64) ([]*CsvRecord, error) {
	file, err := os.Open(filename)
	if err != nil {
		return []*CsvRecord{}, errors.New(fmt.Sprintf("Failed to open file. Error: %v", err))
	}
	defer file.Close()

	return p.ParseReader(file, data, skipRows)
}

//Read csv records and parse each record into the provided data interface
func (p *CsvParser) ParseReader(rdr io.Reader, data interface{}, skipRows int64) ([]*CsvRecord, error) {
	result := []*CsvRecord{}

	reader := csv.NewReader(rdr)
	//Configure reader options Ref http://golang.org/src/pkg/encoding/csv/reader.go?s=#L81
	reader.Comma = p.comma
	reader.FieldsPerRecord = -1 //variable
	reader.TrimLeadingSpace = true

	var lineCount int64 = 1
	for {
		t1 := time.Now()

		record, err := reader.Read()

		if err == io.EOF {
			break
		} else if err != nil {
			//fmt.Println("Error:", err)
			return result, err
		} else if lineCount > skipRows {
			recordSha256 := hashRecord(record)
			r := CsvRecord{
				Record: record,
				Sha256: recordSha256,
				Data:   nil,
				Errors: []string{},
				Ns:     0,
			}

			recordData, err := UnmarshallRecord(record, data)
			if err != nil {
				r.appendError(err.Error())
			}
			r.Data = recordData

			r.Ns = time.Since(t1).Nanoseconds()
			result = append(result, &r)
		}

		lineCount += 1
	}

	return result, nil
}

func hashRecord(record []string) string {
	h := sha256.New()
	h.Write([]byte(fmt.Sprintf("%v", record)))
	bs := h.Sum(nil)
	return hex.EncodeToString(bs)
	//return base64.URLEncoding.EncodeToString(bs)
}

func printReflectValue(rv reflect.Value) {
	log.Printf("rv, type=%s, kind=%s\n", rv.Type().String(), rv.Kind().String())
}

func printReflectType(rt reflect.Type) {
	log.Printf("rt, type=%s, kind=%s\n", rt.Name(), rt.Kind().String())
}

func UnmarshallRecord(record []string, v interface{}) (interface{}, error) {
	//Expect pointer
	rv := reflect.ValueOf(v)
	if rv.Kind() != reflect.Pointer || rv.IsNil() {
		return nil, fmt.Errorf("invalid unmarshall error. Expected pointer, got: %s", rv.Kind().String())
	}

	//Expect pointer to slice
	pointerValue := rv.Elem()
	//rv2 := reflect.ValueOf(pointerValue.Interface())
	rv2Type := reflect.TypeOf(pointerValue.Interface())
	if rv2Type.Kind() != reflect.Slice {
		return nil, fmt.Errorf("invalid unmarshall error. Expected pointer to slice, got pointer to %s", rv2Type.Kind().String())
	}

	//Get type of items in slice
	sliceType := rv2Type.Elem()

	//Expect slice of structs
	if sliceType.Kind() != reflect.Struct {
		return nil, fmt.Errorf("invalid unmarshall error. Expected pointer to slice of structs, got pointer to slice of %s", sliceType.Kind().String())
	}

	//Unmarshall the record and append it to the slice
	unmarshalledRecordPtr, err := unmarshalRecord(record, sliceType)
	if err != nil {
		return nil, err
	}

	return unmarshalledRecordPtr.Interface(), nil
}

/**
https://github.com/gocarina/gocsv/blob/master/unmarshaller.go
https://sosedoff.com/2016/07/16/golang-struct-tags.html
*/
func AppendRecord(record []string, v interface{}) error {
	//Expect pointer
	rv := reflect.ValueOf(v)
	if rv.Kind() != reflect.Pointer || rv.IsNil() {
		return fmt.Errorf("invalid unmarshall error. Expected pointer, got: %s", rv.Kind().String())
	}

	//Expect pointer to slice
	pointerValue := rv.Elem()
	/*
		//rv2 := reflect.ValueOf(pointerValue.Interface())
		rv2Type := reflect.TypeOf(pointerValue.Interface())
		if rv2Type.Kind() != reflect.Slice {
			return fmt.Errorf("invalid unmarshall error. Expected pointer to slice, got pointer to %s", rv2Type.Kind().String())
		}

		//Get type of items in slice
		sliceType := rv2Type.Elem()

		//Expect slice of structs
		if sliceType.Kind() != reflect.Struct {
			return fmt.Errorf("invalid unmarshall error. Expected pointer to slice of structs, got pointer to slice of %s", sliceType.Kind().String())
		}

		//Unmarshall the record and append it to the slice
		unmarshalledRecordPtr, err := unmarshalRecord(record, sliceType)

	*/
	unmarshalledRecordPtr, err := UnmarshallRecord(record, v)
	if err != nil {
		return fmt.Errorf("failed to unmarshall record: %s", err)
	} else {
		//https://stackoverflow.com/questions/42494333/appending-to-go-lang-slice-using-reflection
		//pointerValue.Set(reflect.Append(pointerValue, reflect.ValueOf(unmarshalledRecordPtr.Elem().Interface())))
		pointerValue.Set(reflect.Append(pointerValue, reflect.ValueOf(unmarshalledRecordPtr))) //.Elem().Interface())))
	}

	return nil

}

type TagConfig struct {
	Idx      int
	Optional bool
	Skip     bool
	Formats  []string
}

func getFieldTagConfig(fieldTag string) *TagConfig {
	tags := TagConfig{Formats: []string{}}
	kvRegexp := regexp.MustCompile("(.+)=(.+)")
	fieldTags := strings.Split(fieldTag, tagSeparator)
	for _, fieldTag := range fieldTags {
		m := kvRegexp.FindStringSubmatch(fieldTag)
		if m != nil {
			key := m[1]
			switch key {
			case "idx":
				v, err := strconv.Atoi(m[2])
				if err != nil {

				} else {
					tags.Idx = v
				}
			case "format":
				tags.Formats = append(tags.Formats, m[2])
			}
		} else if fieldTag == "optional" {
			tags.Optional = true
		} else if fieldTag == "-" {
			tags.Skip = true
		}
	}

	if len(tags.Formats) <= 0 {
		tags.Formats = append(tags.Formats, "2006-01-02")
	}
	return &tags
}

func unmarshalRecord(record []string, t reflect.Type) (reflect.Value, error) {
	newValuePtr := reflect.New(t)

	fieldsCount := t.NumField()
	for i := 0; i < fieldsCount; i++ {
		field := t.Field(i)
		fieldTagConfig := getFieldTagConfig(field.Tag.Get(tagName))
		recordValue := record[fieldTagConfig.Idx]
		switch field.Type.Kind() {
		case reflect.Int64:
			if err := setInt64Value(newValuePtr, i, recordValue); err != nil {
				return newValuePtr, err
			}
		case reflect.Float64:
			if err := setFloat64Value(newValuePtr, i, recordValue); err != nil {
				return newValuePtr, err
			}
		case reflect.String:
			if err := setStringValue(newValuePtr, i, recordValue); err != nil {
				return newValuePtr, err
			}
		case reflect.Struct:
			f := reflect.Indirect(newValuePtr).Field(i)
			fieldValue := f.Interface()
			if _, ok := fieldValue.(time.Time); ok {
				if err := setTimestampValue(newValuePtr, i, recordValue, fieldTagConfig.Formats); err != nil {
					return newValuePtr, err
				}
			} else {
				return newValuePtr, fmt.Errorf("unsupported struct type. type = %s\n", reflect.TypeOf(fieldValue).String())
			}
		}
	}

	return newValuePtr, nil
}

func setStringValue(valuePtr reflect.Value, fieldIdx int, recordValue string) error {
	normalizedRecordValue := strings.Trim(recordValue, " \t\n\r")
	valuePtr.Elem().Field(fieldIdx).SetString(normalizedRecordValue)
	return nil
}

func setInt64Value(valuePtr reflect.Value, fieldIdx int, recordValue string) error {
	var result int64 = 0
	var mod int64 = 1

	if recordValue != "" {
		if strings.HasPrefix(recordValue, "-") {
			mod = -1
			recordValue = strings.TrimPrefix(recordValue, "-")
		}
		normalizedValue := strings.TrimPrefix(recordValue, "€")
		normalizedValue = strings.Trim(normalizedValue, " \t")
		val, err := strconv.ParseInt(normalizedValue, 10, 64)
		if err != nil {
			return fmt.Errorf("failed to parse value [%s] as int", normalizedValue)
		}

		if err != nil {
			return err
		}

		result = val * mod
	}

	valuePtr.Elem().Field(fieldIdx).SetInt(result)
	return nil
}

func setFloat64Value(valuePtr reflect.Value, fieldIdx int, recordValue string) error {
	var result float64 = 0.0

	if recordValue != "" {
		mod := 1.0
		normalizedValue := recordValue
		if strings.HasPrefix(normalizedValue, "-") {
			mod = -1.0
			normalizedValue = strings.TrimPrefix(normalizedValue, "-")
		} else if strings.HasPrefix(normalizedValue, "+") {
			mod = 1.0
			normalizedValue = strings.TrimPrefix(normalizedValue, "+")
		}
		normalizedValue = strings.Replace(normalizedValue, ".", "", -1)
		normalizedValue = strings.Replace(normalizedValue, ",", ".", -1)
		normalizedValue = strings.TrimPrefix(normalizedValue, "€")
		normalizedValue = strings.Trim(normalizedValue, " \t")
		val, err := strconv.ParseFloat(normalizedValue, 64)
		if err != nil {
			return fmt.Errorf("failed to parse value [%s] as float64. Error: %s", normalizedValue, err)
		}

		result = val * mod
	}

	valuePtr.Elem().Field(fieldIdx).SetFloat(result)
	return nil
}

func setTimestampValue(valuePtr reflect.Value, fieldIdx int, recordValue string, recordValueFormats []string) error {
	if recordValue != "" {
		//Try to parse the timestamp with any of the supplied formats
		var err error
		var timestamp time.Time
		for _, recordValueFormat := range recordValueFormats {
			timestamp, err = time.Parse(recordValueFormat, recordValue)
			if err == nil {
				break
			}
		}
		//Return error or value
		if err != nil {
			return fmt.Errorf("failed to parse timestamp for raw value: %s. error=%s", recordValue, err)
		}
		valuePtr.Elem().Field(fieldIdx).Set(reflect.ValueOf(timestamp))
	}
	return nil
}

/*
func test(v interface{}) error {
	//Expect pointer
	rv := reflect.ValueOf(v)
	log.Printf("rv, type=%s, kind=%s\n", rv.Type().String(), rv.Kind().String())
	if rv.Kind() != reflect.Pointer || rv.IsNil() {
		return fmt.Errorf("Invalid unmarshall error. Expected pointer, got: %s", rv.Kind().String())
	}

	//Expect pointer to slice
	pointerValue := rv.Elem()
	//rv2 := reflect.ValueOf(pointerValue.Interface())
	rv2Type := reflect.TypeOf(pointerValue.Interface())
	if rv2Type.Kind() != reflect.Slice {
		return fmt.Errorf("Invalid unmarshall error. Expected pointer to slice, got pointer to %s", rv2Type.Kind().String())
	}

	//Get type of items in slice
	sliceType := rv2Type.Elem()

	//Expect slice of structs
	if sliceType.Kind() != reflect.Struct {
		return fmt.Errorf("Invalid unmarshall error. Expected pointer to slice of structs, got pointer to slice of %s", sliceType.Kind().String())
	}

	log.Printf("Slice type=%s\n", sliceType.Name())

	newValuePtr := reflect.New(sliceType)

	fieldsCount := sliceType.NumField()
	for i := 0; i < fieldsCount; i++ {
		field := sliceType.Field(i)
		log.Printf("Field idx=%d, name=%s, type=%s\n", i, field.Name, field.Type)

		switch field.Type.Kind() {
		case reflect.Int64:
			newValuePtr.Elem().Field(i).SetInt(rand.Int63())
		case reflect.Float64:
			newValuePtr.Elem().Field(i).SetFloat(rand.Float64() * 100.0)
		case reflect.String:
			newValuePtr.Elem().Field(i).SetString("test")
		}
	}

	//https://stackoverflow.com/questions/42494333/appending-to-go-lang-slice-using-reflection
	pointerValue.Set(reflect.Append(pointerValue, reflect.ValueOf(newValuePtr.Elem().Interface())))

	return nil
}
*/

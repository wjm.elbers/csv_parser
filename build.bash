#!/usr/bin/env bash

GO_PATH="/go"
build_image="docker-golang-build:5e197fe"

build() {
  project_path=${1}
  binary="${2}"

  echo "  Building ${binary}"
  docker run --rm \
    -e "GOPATH=${GO_PATH}" \
    -e "BINARY=${binary}" \
    -v "$PWD":/go/src \
    -v "$PWD":/output \
    -w /go/src \
    ${build_image} sh -c 'make'
}

build_all_binaries() {
  #Sourced via build script, so no arrays supported
  set -e
  echo "Building binaries:"
  build "backend" "backend"
  set +e
}

main() {
  build_all_binaries
}

main "$@"; exit